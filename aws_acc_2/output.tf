output "vpc_details" {
  description = "vpc_id, igw"
  value       = module.vpc.vpc_details
}

output "database_subnets" {
  description = "local db subnets"
  value = module.database_subnets.subnet_outputs
}

output "cache_subnets" {
  description = "local cache subnet"
  value = module.cache_subnets.subnet_outputs
}

output "rmt_database_subnets" {
  description = "remote subnet"
  value = module.rmt_database_subnets.subnet_outputs
}