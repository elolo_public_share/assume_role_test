terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.67.0"
    }
  }
}

// local account provider
provider "aws" {
  region = var.region
  assume_role {
    role_arn ="arn:aws:iam::${var.aws_account}:role/${var.my_iam_role}"
  }
}

// remote account provider
provider "aws" {
  alias = "rmt_account"
  region = var.rmt_region
  assume_role {
    role_arn ="arn:aws:iam::${var.rmt_aws_account}:role/${var.my_iam_role}"
  }
}