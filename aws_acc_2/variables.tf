// GENERAL VARIABLES
variable "aws_account" {
  type = string
  default = "068054933511"
}

variable "rmt_aws_account" {
  type = string
  default = "068414940364"
}

variable "region" {
  default = "us-west-2"
}

variable "rmt_region" {
  default = "us-east-1"
}


variable "tags" {
    default = {
        Created_by = "mkc"
    } 
}

variable "rmt_tags" {
    default = {
        Created_by = "rmt"
    } 
}


// VPC VARIABLES
variable "vpc_name" {
  default = "lab_2_vpc"
}

variable "vpc_cidr" {
  default = "172.22.0.0/16"
}


// SUBNET AND AZ

locals {
  database_subnets = {
    mkc_database_subnet_1 = ["172.22.10.0/24", "${var.region}a"]
    mkc_database_subnet_2 = ["172.22.11.0/24", "${var.region}b"]
  }

  cache_subnets = {
    mkc_cache_subnet_1 = ["172.22.15.0/24", "${var.region}a"]
    mkc_cache_subnet_2 = ["172.22.16.0/24", "${var.region}b"]
  }
}

// CROSS ACCOUNT SUBNET
locals {
    remote_database_subnets = {
    rmt_database_subnet_1 = ["172.20.10.0/24", "${var.rmt_region}a"]
    rmt_database_subnet_2 = ["172.20.11.0/24", "${var.rmt_region}b"]
  }
}
