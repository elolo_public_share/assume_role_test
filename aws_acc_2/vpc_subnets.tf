#------------------------------------------------------------------------------------------------------------------------------------------
# GENERIC VPC
#------------------------------------------------------------------------------------------------------------------------------------------
module "vpc" {
  source   = "../modules/vpc_v2"
  vpc_name = var.vpc_name
  vpc_cidr = var.vpc_cidr
  igw      = false

  tags = var.tags

}

#------------------------------------------------------------------------------------------------------------------------------------------
# GENERIC SUBNETs
#------------------------------------------------------------------------------------------------------------------------------------------

module "database_subnets" {
  source  = "../modules/subnet_v2"
  vpc_id  = module.vpc.vpc_details.vpc_id
  subnets = local.database_subnets
  tags    = var.tags
}

module "cache_subnets" {
  source  = "../modules/subnet_v2"
  vpc_id  = module.vpc.vpc_details.vpc_id
  subnets = local.cache_subnets
  tags    = var.tags
}


#------------------------------------------------------------------------------------------------------------------------------------------
# REMOTE SUBNETs
#------------------------------------------------------------------------------------------------------------------------------------------
// Query remote vpc id
data "terraform_remote_state" "vpc_acc_1" {
  backend = "local"
  config = {
    path = "${path.module}/../aws_acc_1/terraform.tfstate"
  }
}

// thi will create subnets in the remote accounts the alias provider
module "rmt_database_subnets" {
  source = "../modules/subnet_v2"
  providers = {
    aws = aws.rmt_account
  }
  vpc_id  = data.terraform_remote_state.vpc_acc_1.outputs.vpc_details["vpc_id"]
  subnets = local.remote_database_subnets
  tags    = var.rmt_tags
}



