// GENERAL VARIABLES
variable "aws_account" {
  description = "aws account to create this resources"
  type = string
  default = "068414940364"
}

variable "region" {
  default = "us-east-1"
}


variable "tags" {
    default = {
        Created_by = "mkc"
    }
  
}


// VPC VARIABLES
variable "vpc_name" {
  default = "lab_1_vpc"
}

variable "vpc_cidr" {
  default = "172.20.0.0/16"
}


// SUBNET AND AZ

locals {
  public_subnets = {
    mkc_pub_subnet_1 = ["172.20.253.0/24", "${var.region}a"]
    mkc_pub_subnet_2 = ["172.20.254.0/24", "${var.region}b"]
  }

  private_subnets = {
    mkc_priv_subnet_1 = ["172.20.0.0/24", "${var.region}a"]
    mkc_priv_subnet_2 = ["172.20.1.0/24", "${var.region}b"]
  }

}
