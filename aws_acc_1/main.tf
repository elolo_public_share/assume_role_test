terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.67.0"
    }
  }
}

provider "aws" {
  region = var.region
  assume_role {
    role_arn ="arn:aws:iam::${var.aws_account}:role/${var.my_iam_role}"
  }
}
