#------------------------------------------------------------------------------------------------------------------------------------------
# GENERIC VPC
#------------------------------------------------------------------------------------------------------------------------------------------
module "vpc" {
  source   = "../modules/vpc_v2"
  vpc_name = var.vpc_name
  vpc_cidr = var.vpc_cidr
  tags     = var.tags

}

#------------------------------------------------------------------------------------------------------------------------------------------
# GENERIC SUBNETs
#------------------------------------------------------------------------------------------------------------------------------------------
module "public_subnets" {
  source  = "../modules/subnet_v2"
  vpc_id  = module.vpc.vpc_details.vpc_id
  subnets = local.public_subnets
  tags    = var.tags
}

module "private_subnets" {
  source  = "../modules/subnet_v2"
  vpc_id  = module.vpc.vpc_details.vpc_id
  subnets = local.private_subnets
  tags    = var.tags
}


