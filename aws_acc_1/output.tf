output "vpc_details" {
  description = "vpc_id, igw"
  value       = module.vpc.vpc_details
}

output "private_subnets" {
  value = module.private_subnets.subnet_outputs
}

output "public_subnets" {
  value = module.public_subnets.subnet_outputs
}

